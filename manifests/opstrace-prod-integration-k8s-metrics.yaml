apiVersion: v1
kind: Namespace
metadata:
  name: opstrace
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: opstrace-prometheus
  namespace: opstrace
data:
  prometheus.yml: |-
    remote_write:
    - url: https://cortex.prod.k8s.nagyv.com/api/v1/push
      authorization:
        credentials_file: /var/run/tenant-auth/token

    scrape_configs:
    # Collection of per-pod metrics
    - job_name: 'kubernetes-pods'
      kubernetes_sd_configs:
      - role: pod

      # TODO should this one be removed? if so then the 'not querying the pods themselves' comment is wrong
      # TLS config for getting pod info, not querying pods themselves
      tls_config:
        ca_file: /var/run/secrets/kubernetes.io/serviceaccount/ca.crt
      authorization:
        credentials_file: /var/run/secrets/kubernetes.io/serviceaccount/token

      relabel_configs:
      # Include <namespace>/<pod name>
      - action: replace
        source_labels: [__meta_kubernetes_namespace, __meta_kubernetes_pod_name]
        separator: /
        replacement: $1
        target_label: job
      # Include namespace
      - action: replace
        source_labels: [__meta_kubernetes_namespace]
        target_label: namespace
      # Include pod name without namespace
      - action: replace
        source_labels: [__meta_kubernetes_pod_name]
        target_label: instance
      # Include parent replicaset/statefulset/daemonset name
      - action: replace
        source_labels: [__meta_kubernetes_pod_controller_name]
        target_label: controller
      # Include container name within pod
      - action: replace
        source_labels: [__meta_kubernetes_pod_container_name]
        target_label: container
      # Include node name
      - action: replace
        source_labels: [__meta_kubernetes_pod_node_name]
        target_label: node

      # Include integration ID for separation in opstrace
      - source_labels: []
        target_label: integration_id
        replacement: '49b85cd3-243e-4548-8115-4bd56fad5065'

      # Internal labels used by prometheus itself
      # Always use HTTPS for scraping the api server
      - source_labels: [__meta_kubernetes_service_label_component]
        regex: apiserver
        action: replace
        target_label: __scheme__
        replacement: https

    # Collection of metrics about the kubelets themselves (request queues, pod launches, etc)
    - job_name: 'kubernetes-nodes'
      kubernetes_sd_configs:
      - role: node

      scheme: https
      # TLS config for getting list of nodes to scrape, not querying nodes themselves
      tls_config:
        ca_file: /var/run/secrets/kubernetes.io/serviceaccount/ca.crt
      authorization:
        credentials_file: /var/run/secrets/kubernetes.io/serviceaccount/token

      relabel_configs:
      - target_label: __scheme__
        replacement: https
      # Include node hostname
      - source_labels: [__meta_kubernetes_node_label_kubernetes_io_hostname]
        target_label: instance
      # Include job label for the nodes
      - source_labels: []
        target_label: job
        replacement: kubelet
      # Include integration ID for autodetection in opstrace
      - source_labels: []
        target_label: integration_id
        replacement: '49b85cd3-243e-4548-8115-4bd56fad5065'

    # Collection of full container resource usage metrics from kubelets
    - job_name: 'kubernetes-cadvisor'
      kubernetes_sd_configs:
      - role: node

      metrics_path: /metrics/cadvisor
      scheme: https
      # TLS config for getting list of nodes to scrape, not querying nodes themselves
      tls_config:
        ca_file: /var/run/secrets/kubernetes.io/serviceaccount/ca.crt
      authorization:
        credentials_file: /var/run/secrets/kubernetes.io/serviceaccount/token

      relabel_configs:
      - target_label: __scheme__
        replacement: https
      # Include node hostname
      - source_labels: [__meta_kubernetes_node_label_kubernetes_io_hostname]
        target_label: instance
      # Include job label for the nodes
      - source_labels: []
        target_label: job
        replacement: kubelet-cadvisor
      # Include integration ID for autodetection in opstrace
      - source_labels: []
        target_label: integration_id
        replacement: '49b85cd3-243e-4548-8115-4bd56fad5065'

    # Collection of summary container resource usage metrics from kubelets
    - job_name: 'kubernetes-resource'
      kubernetes_sd_configs:
      - role: node

      metrics_path: /metrics/resource
      scheme: https
      # TLS config for getting list of nodes to scrape, not querying nodes themselves
      tls_config:
        ca_file: /var/run/secrets/kubernetes.io/serviceaccount/ca.crt
      authorization:
        credentials_file: /var/run/secrets/kubernetes.io/serviceaccount/token

      relabel_configs:
      - target_label: __scheme__
        replacement: https
      # Include node hostname
      - source_labels: [__meta_kubernetes_node_label_kubernetes_io_hostname]
        target_label: instance
      # Include job label for the nodes
      - source_labels: []
        target_label: job
        replacement: kubelet-resource
      # Include integration ID for autodetection in opstrace
      - source_labels: []
        target_label: integration_id
        replacement: '49b85cd3-243e-4548-8115-4bd56fad5065'

    # Collection of readinessProbe/livenessProbe/etc stats from kubelets
    - job_name: 'kubernetes-probes'
      kubernetes_sd_configs:
      - role: node

      metrics_path: /metrics/probes
      scheme: https
      # TLS config for getting list of nodes to scrape, not querying nodes themselves
      tls_config:
        ca_file: /var/run/secrets/kubernetes.io/serviceaccount/ca.crt
      authorization:
        credentials_file: /var/run/secrets/kubernetes.io/serviceaccount/token

      relabel_configs:
      - target_label: __scheme__
        replacement: https
      # Include node hostname
      - source_labels: [__meta_kubernetes_node_label_kubernetes_io_hostname]
        target_label: instance
      # Include job label for the nodes
      - source_labels: []
        target_label: job
        replacement: kubelet-probes
      # Include integration ID for autodetection in opstrace
      - source_labels: []
        target_label: integration_id
        replacement: '49b85cd3-243e-4548-8115-4bd56fad5065'

    # Collection of the kubernetes apiserver
    - job_name: 'kubernetes-service'
      kubernetes_sd_configs:
      - role: endpoints

      # TLS config for getting endpoint info, not querying nodes themselves
      tls_config:
        ca_file: /var/run/secrets/kubernetes.io/serviceaccount/ca.crt
      authorization:
        credentials_file: /var/run/secrets/kubernetes.io/serviceaccount/token

      relabel_configs:
      - source_labels: [__meta_kubernetes_service_label_component]
        regex: apiserver
        action: keep
      - target_label: __scheme__
        replacement: https
      # Include job label for the service
      - source_labels: []
        target_label: job
        replacement: apiserver
      # Include integration ID for autodetection in opstrace
      - source_labels: []
        target_label: integration_id
        replacement: '49b85cd3-243e-4548-8115-4bd56fad5065'
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: opstrace-prometheus
  namespace: opstrace
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: opstrace-prometheus
rules:
- apiGroups:
  - ""
  resources:
  - nodes
  - nodes/metrics
  - nodes/proxy
  - services
  - endpoints
  - pods
  verbs:
  - get
  - list
  - watch
- nonResourceURLs:
  - /metrics
  verbs:
  - get
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: opstrace-prometheus
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: opstrace-prometheus
subjects:
- kind: ServiceAccount
  name: opstrace-prometheus
  namespace: opstrace
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: opstrace-prometheus
  namespace: opstrace
spec:
  replicas: 1
  selector:
    matchLabels:
      name: opstrace-prometheus
  template:
    metadata:
      labels:
        name: opstrace-prometheus
    spec:
      serviceAccountName: opstrace-prometheus
      containers:
      - name: prometheus
        image: prom/prometheus:v2.26.0 # TODO replace with grafana-agent
        imagePullPolicy: IfNotPresent
        args:
        - --config.file=/etc/prometheus/prometheus.yml
        ports:
        - name: ui
          containerPort: 9090
        volumeMounts:
        # Prometheus configmap
        - name: config
          mountPath: /etc/prometheus
        # Opstrace tenant auth secret
        - name: tenant-auth
          mountPath: /var/run/tenant-auth
          readOnly: true
      volumes:
        - name: config
          configMap:
            name: opstrace-prometheus
        - name: tenant-auth
          secret:
            secretName: opstrace-tenant-auth
